#include <Arduino.h>

// This sketch is using the clockwise pin mapping
#ifdef PINMAPPING_CCW
#error "Sketch was written for clockwise pin mapping!"
#endif

void setup() {
    pinMode(PA3, OUTPUT);
}

void loop() {
    digitalWrite(PA3, HIGH);
    delay(500);
    digitalWrite(PA3, LOW);
    delay(500);
}
