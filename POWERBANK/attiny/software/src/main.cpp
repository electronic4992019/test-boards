#include "main.h"

void setup(void) {
    EEPROM.begin();
    EEPROM.get(0, pump_interval);

    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, LOW); // Turn LED off

    pinMode(PIN_PUMP, OUTPUT);
    digitalWrite(PIN_PUMP, LOW); // Turn pump off

    pinMode(PIN_BUTTON, INPUT_PULLUP);
    if (digitalRead(PIN_BUTTON) == LOW) {
        mode_mesuring = true;
        pump_interval = 0;
        digitalWrite(PIN_LED, HIGH); // Turn LED on
    } else {
        mode_mesuring = false;
        digitalWrite(PIN_LED, LOW); // Turn LED off
    }
}

void loop(void) {
    if (mode_mesuring) {
        loop_mesuring();
    } else {
        loop_running();
    }
}

void loop_mesuring(void) {
    snore(4800);
    pump_interval += 5;
    EEPROM.put(0, pump_interval);
    digitalWrite(PIN_LED, LOW); // Turn LED off
    delay(100);
    digitalWrite(PIN_LED, HIGH); // Turn LED on
}

void loop_running(void) {
    // If the pump_interval is 0, do nothing just blink the LED
    if (pump_interval == 0) {
        digitalWrite(PIN_LED, HIGH); // Turn LED on
        delay(500);
        digitalWrite(PIN_LED, LOW); // Turn LED off
        delay(500);
    } else {
        snore(pump_interval * 1000 - 500); // keep 500 ms as security
        digitalWrite(PIN_PUMP, HIGH); // Turn pump on
        delay(10);
        digitalWrite(PIN_PUMP, LOW); // Turn pump off
        digitalWrite(PIN_LED, HIGH); // Turn LED on
        delay(50);
        digitalWrite(PIN_LED, LOW); // Turn LED off
    }
}
