#ifndef SOFTWARE_MAIN_H
#define SOFTWARE_MAIN_H


#include <Arduino.h>
#include "tinysnore.h"
#include "EEPROM.h"

// Pin definitions
#define PIN_LED PB3
#define PIN_PUMP PB4
#define PIN_BUTTON PB1  // Button is shared with MISO ISP Programmer

// global variables
bool mode_mesuring = false;
uint8_t pump_interval = 0;


// Function prototypes
void setup(void);
void loop(void);

void loop_mesuring(void);
void loop_running(void);

#endif //SOFTWARE_MAIN_H
