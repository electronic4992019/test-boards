#include "ch32v003fun.h"
#include <stdio.h>

#define LED_PIN PD6

int main() {
    SystemInit();

    // Enable GPIOs
    funGpioInitAll();

    funPinMode(LED_PIN, GPIO_Speed_10MHz | GPIO_CNF_OUT_PP);


    while (1) {
        funDigitalWrite(LED_PIN, FUN_HIGH);
        Delay_Ms(1000);
        funDigitalWrite(LED_PIN, FUN_LOW);
        Delay_Ms(1000);
    }
}
