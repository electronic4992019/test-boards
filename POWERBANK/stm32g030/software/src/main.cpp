#include "main.h"


void setup() {
    Serial.begin(115200);
    LowPower.begin();

    pinMode(PIN_PUMP, OUTPUT);
    digitalWrite(PIN_PUMP, LOW);
    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, LOW);
    pinMode(PIN_BUTTON, INPUT_PULLUP);


    Serial.println("Setup done");
    EEPROM.begin();
    EEPROM.get(0, alive_time);
    if(alive_time < 10){
        alive_time = 10;
    }


    // if button is pressed, enable mesure mode
    // otherwise, start in run mode
    if (digitalRead(PIN_BUTTON) == LOW) {
        Serial.println("Mesure mode enabled");
        is_mesure_mode = true;
        taskManager.schedule(repeatMillis(1000), do_blinkled);
        taskManager.schedule(repeatSeconds(5), write_eeprom);
    } else {
        Serial.print("Run Mode enabled: ");
        Serial.print(alive_time);
        Serial.println(" (s)");
        is_mesure_mode = false;
        taskManager.schedule(repeatSeconds(alive_time), do_blinkled_short);
        taskManager.schedule(repeatSeconds(alive_time), do_pump);
    }
}

void loop() {
    if (is_mesure_mode) {
        loop_mesure();
    } else {
        loop_run();
    }
}

void loop_mesure() {
    taskManager.runLoop();
}

void loop_run() {
    taskManager.runLoop();
    delay_to_next_event = taskManager.microsToNextTask();
    if (delay_to_next_event > 200) {
        LowPower.deepSleep(delay_to_next_event);
    }
}

void do_blinkled() {
    digitalWrite(PIN_LED, led_status);
    led_status = !led_status;
}

void do_blinkled_short() {
    digitalWrite(PIN_LED, HIGH);
    delay(100);
    digitalWrite(PIN_LED,LOW);
}

void write_eeprom() {
    alive_time += 5;
    EEPROM.put(0, alive_time);
}

void do_pump() {
    digitalWrite(PIN_PUMP, LOW);
    delay(10);
    digitalWrite(PIN_PUMP,HIGH);
    Serial.print("Pumping done");
}

