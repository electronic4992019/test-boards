#ifndef SOFTWARE_MAIN_H
#define SOFTWARE_MAIN_H

#include "Arduino.h"
#include "TaskManagerIO.h"
#include "STM32LowPower.h"
#include "EEPROM.h"

// Pin definitions
#define PIN_LED PA2
#define PIN_PUMP PA3
#define PIN_BUTTON PA4

// global variables
bool is_mesure_mode = false;
bool led_status = LOW;
int alive_time = 10;
uint32_t delay_to_next_event;

// function prototypes
void setup();
void loop();

void loop_mesure();
void loop_run();

void do_blinkled();
void do_blinkled_short();
void write_eeprom();
void do_pump();

#endif //SOFTWARE_MAIN_H
