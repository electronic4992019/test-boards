# POWERBANK

Petit circuit permettant de maintenir en fonction un powerbank. En effet, elles se coupent si l'on ne consomme pas suffisament de courant (probablement autour des 50 mA)

Le circuit présente deux modes de fonctionnement :

1. Si il est branché sur la powerbank avec le bouton pressé alors il va mesurer le temps nécessaire avant de devoir délivrer une impulsion
2. Lorsqu'il est simplement branché, il va prendre la dernière valeur enregistrée en mémoire et va envoyer une impulsion afin de garder la batterie en fonction.

## PCB

Le design du PCB a été effectué autour d'un ATTINY45. 

![](attiny/documents/powerbank-pcb.jpg)

Les traces les plus fines font 0.25mm comme illustré ci-après:

![alt text](attiny/documents/powerbank-pcb-detail.jpg)


![alt text](attiny/documents/powerbank-attiny-pcb-design.jpg)