# ATTINYx14 (Attiny1614) Breakout board

A small board for breaking out ATTINY 1614

All pins are exposed

![board](documents/attiny1614_breakout.jpg)

## Pinouts

see : https://github.com/SpenceKonde/megaTinyCore for more information.

![alt text](documents/ATtiny_x14_pinout.gif)