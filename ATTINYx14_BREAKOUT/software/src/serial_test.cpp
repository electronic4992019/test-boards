#include <Arduino.h>

#define PIN_LED_1 PIN_PA3
#define PIN_LED_2 PIN_PB0

void setup() {
    pinMode(PIN_LED_1, OUTPUT);
    pinMode(PIN_LED_2, OUTPUT);
    digitalWrite(PIN_LED_1, LOW);
    digitalWrite(PIN_LED_2, LOW);
    Serial.begin(9600);
}

void loop() {
    digitalWrite(PIN_LED_1, HIGH);
    digitalWrite(PIN_LED_2, HIGH);
    Serial.println("Hello World!");
    Serial.flush();
    delay(1500);
    digitalWrite(PIN_LED_1, LOW);
    digitalWrite(PIN_LED_2, LOW);
    delay(1500);
}