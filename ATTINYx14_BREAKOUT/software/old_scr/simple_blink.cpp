#include <Arduino.h>

#define PIN_LED_1 PIN_PA3

void setup() {
    pinMode(PIN_LED_1, OUTPUT);
    digitalWrite(PIN_LED_1, LOW);
}

void loop() {
    digitalWrite(PIN_LED_1, HIGH);
    delay(1000);
    digitalWrite(PIN_LED_1, LOW);
    delay(1000);
}