#include "stm8s.h"

#define LED_GPIO_PORT  (GPIOD)
#define LED_GPIO_PINS  (GPIO_PIN_6)

#define Led_Init GPIO_Init(LED_GPIO_PORT, LED_GPIO_PINS, GPIO_MODE_OUT_PP_LOW_FAST)
#define Led_ON   GPIO_WriteHigh    (LED_GPIO_PORT,LED_GPIO_PINS)
#define Led_OFF  GPIO_WriteLow     (LED_GPIO_PORT,LED_GPIO_PINS)
#define Led_TOG  GPIO_WriteReverse (LED_GPIO_PORT,LED_GPIO_PINS)

void main(void)
{

    // Init LED Port, Pin
    Led_Init;

    // Set LED ON
    Led_ON;

    // Loop
    while(1){
        // Toggle LED ON/OFF
        Led_TOG;

        // White moment
        for(uint16_t d = 0; d<19000; d++){
            for(uint8_t c = 0; c<5; c++);
        }
    }
}

#ifdef USE_FULL_ASSERT
void assert_failed(uint8_t* file, uint32_t line)
{
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

  /* Infinite loop */
  while (1)
  {
  }
}
#endif
