/* taskmanagerio is not working with stm8s003f3p.
 * because stm8 didn't support c++ code, but only c code. */

#include <Arduino.h>
#define PIN_LED PC6

void setup() {
    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, LOW);
}

void loop() {
    digitalWrite(PIN_LED, CHANGE);
    delay(1000);
}