#include "stm8s.h"

void GPIO_setup(void);
void Delay (uint16_t nCount);

void main(void) {
    bool i = FALSE;
    GPIO_setup();
    for (;;) {
        if (GPIO_ReadInputPin(GPIOB, GPIO_PIN_7) == FALSE) {
            while (GPIO_ReadInputPin(GPIOB, GPIO_PIN_7) == FALSE);
            i ^= 1;
        }
        switch (i) {
            case TRUE: {
                Delay(0xFFF);
                break;
            }
            case FALSE: {
                Delay(0xFF);
                break;
            }
        }
        GPIO_WriteReverse(GPIOD, GPIO_PIN_0);
    };
}

void GPIO_setup(void) {
    GPIO_DeInit(GPIOB);
    GPIO_Init(GPIOB, GPIO_PIN_7, GPIO_MODE_IN_FL_NO_IT);
    GPIO_DeInit(GPIOD);
    GPIO_Init(GPIOD, GPIO_PIN_0, GPIO_MODE_OUT_PP_LOW_FAST);
}


void Delay(uint16_t nCount)
{
    /* Decrement nCount value */
    while (nCount != 0)
    {
        nCount--;
    }
}

/**
  * @brief  Reports the name of the source file and the source line number
  *   where the assert_param error has occurred.
  * @param file: pointer to the source file name
  * @param line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{
    /* User can add his own implementation to report the file name and line number,
       ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */

    /* Infinite loop */
    while (1)
    {
    }
}
