#pragma once

void WS2812_send_data(unsigned char r, unsigned char g, unsigned char b);
void WS2812_reset(void);
void WS2812_set_common_colour_on_all_LEDs(unsigned char r, unsigned char g, unsigned char b);
void WS2812_update(void);
void setup(void);
void loop(void);