/* Test code for buzzer

 see ../../document/buzzer_schema.png for wiring */

#include <Arduino.h>

#define PIN_BUZZER PA3
#define PIN_LED PC6

unsigned long previousMillis = 0;  // will store last time LED was updated
const long interval = 1000;        // interval at which to blink (milliseconds)
int ledState = LOW;                // ledState used to set the LED

void blink_led() {
    ledState = !ledState;
    digitalWrite(PIN_LED, ledState);
}

void play_buzzer() {
    tone(PIN_BUZZER, 1400);
    delay(200);
    tone(PIN_BUZZER, 500);
    delay(200);
    noTone(PIN_BUZZER);
}

void do_every_interval() {
    unsigned long currentMillis = millis();
    if (currentMillis - previousMillis >= interval) {
        // save the last time you blinked the LED
        previousMillis = currentMillis;

        blink_led();
        play_buzzer();
    }
}

void setup() {
    pinMode(PIN_LED, OUTPUT);
    pinMode(PIN_BUZZER, OUTPUT);
    digitalWrite(PIN_BUZZER, LOW);
    digitalWrite(PIN_LED, LOW);
}

void loop() {
    do_every_interval();
}