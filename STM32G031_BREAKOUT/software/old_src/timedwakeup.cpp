#include <Arduino.h>
#include "STM32LowPower.h"

// PINS DEFINITION
#define PIN_LED PC6

void setup() {
    pinMode(PIN_LED, OUTPUT);
    // Configure low power
    LowPower.begin();
}

void loop() {
    digitalWrite(PIN_LED, HIGH);
    LowPower.deepSleep(1000);
    digitalWrite(LED_BUILTIN, LOW);
    LowPower.deepSleep(1000);
}
