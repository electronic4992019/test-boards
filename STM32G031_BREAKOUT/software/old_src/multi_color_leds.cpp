#include <Arduino.h>

#include "HardwareSerial.h"
#include "STM32LowPower.h"
#include "TaskManagerIO.h"
//#include "Adafruit_NeoPixel.h"

#define PIN_LED PC6
#define PIN_BUTTON PA0
#define PIN_LED_CONTROL PA2
#define NUM_LEDS 1

// Set the SPI clock divisor to as close to 400ns as we can;
// the WS2812 spec allows for +/- 150ns

#define WS2812B_SPI_DIVISOR SPI_CLOCK_DIV32  // 500ns

//Adafruit_NeoPixel pixels(NUM_LEDS, PIN_LED_CONTROL, NEO_GRB + NEO_KHZ800);
#define DELAYVAL 500


void setup() {
    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, LOW);
    pinMode(PIN_BUTTON, INPUT_PULLUP);
    //pinMode(PIN_LED_CONTROL, OUTPUT);
    //pixels.begin();

    TIM_TypeDef *Instance = (TIM_TypeDef *)pinmap_peripheral(digitalPinToPinName(PIN_LED_CONTROL), PinMap_PWM);
    uint32_t channel = STM_PIN_CHANNEL(pinmap_function(digitalPinToPinName(PIN_LED_CONTROL), PinMap_PWM));
    // Instantiate HardwareTimer object. Thanks to 'new' instantiation, HardwareTimer is not destructed when setup() function is finished.
    HardwareTimer *MyTim = new HardwareTimer(Instance);
    // Configure and start PWM
    MyTim->setPWM(channel, PIN_LED_CONTROL, 5, 10); // 5 Hertz, 10% dutycycle


}

void loop() {
    digitalWrite(PIN_LED, LOW);
   // digitalWrite(PIN_LED_CONTROL, LOW);
    delay(DELAYVAL);


//    pixels.clear();
//    pixels.setPixelColor(0, pixels.Color(0, 150, 0));
//    pixels.show();

   // digitalWrite(PIN_LED_CONTROL, HIGH);
    digitalWrite(PIN_LED, HIGH);
    delay(DELAYVAL);
}