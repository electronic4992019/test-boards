#include <Arduino.h>
#include <STM32LowPower.h>
#include <TaskManagerIO.h>

#define PIN_LED PC6
#define PIN_BUTTON PA0

int one_second_pulse_task_id;

void OneSecondPulse() {
    digitalWrite(PIN_LED, HIGH);
    taskManager.yieldForMicros(500 * 1000);
    digitalWrite(PIN_LED, LOW);
}

void Shutdown() {
    taskManager.cancelTask(one_second_pulse_task_id);
    digitalWrite(PIN_LED, LOW);
    LowPower.shutdown();
}

void setup() {
    LowPower.begin();

    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, LOW);

    pinMode(PIN_BUTTON, INPUT_PULLUP);

    one_second_pulse_task_id = taskManager.scheduleFixedRate(1, OneSecondPulse, TIME_SECONDS);
    taskManager.scheduleOnce(10, Shutdown, TIME_SECONDS);
}

void loop() {
    taskManager.runLoop();
}