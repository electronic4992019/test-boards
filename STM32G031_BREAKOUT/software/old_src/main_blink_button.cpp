#include <Arduino.h>
#include <TaskManagerIO.h>
#include <STM32LowPower.h>

// PINS DEFINITION
#define PIN_LED PC6
#define PIN_BUTTON PA0

HardwareSerial STMSerial(PB7, PB6);

/**
 * @brief Class to control the blinking of an LED
 */
class BlinkLed : public Executable {
  private:
    bool led_state = false; /**< Current state of the LED */
    bool led_stop = false;  /**< Flag to stop the LED */

  public:
    /**
     * @brief Method to toggle the LED state
     */
    void exec() override {
        if (led_stop) {
            digitalWrite(PIN_LED, false);
        } else {
            digitalWrite(PIN_LED, led_state);
            led_state = !led_state;
            // STMSerial.print("LED state: ");
            // STMSerial.println(led_state);
        }
    }

    /**
     * @brief Method to set the stop flag for the LED
     * @param ledStop Flag to stop the LED
     */
    void setLedStop(bool ledStop) {
        led_stop = ledStop;
    }
};

BlinkLed led_red;

/**
 * @brief Class to handle events triggered by a button
 */
class EventButton : public BaseEvent {
  private:
    bool stop_led = true; /**< Flag to stop the LED */

  public:
    /**
     * @brief Method to execute the button event
     */
    void exec() override {
        led_red.setLedStop(stop_led);
        stop_led = !stop_led;
    }

    /**
     * @brief Method to return the time of the next check
     * @return Time of the next check
     */
    uint32_t timeOfNextCheck() override {
        return 100000000L;
    }
};

EventButton btn_event;

void onInteruptButton() {
    btn_event.markTriggeredAndNotify();
}

// MAIN FUNCTIONS

/**
 * @brief Method to setup the system
 */
void setup() {
    LowPower.begin();
    STMSerial.begin(115200);

    pinMode(PIN_LED, OUTPUT);
    pinMode(PIN_BUTTON, INPUT_PULLUP);
    digitalWrite(PIN_LED, LOW);

    attachInterrupt(digitalPinToInterrupt(PIN_BUTTON), onInteruptButton, FALLING);

    taskManager.schedule(repeatMillis(1000), &led_red);
    taskManager.registerEvent(&btn_event);
}

/**
 * @brief Loop method, only run the task manager
 */
void loop() {
    auto milli_delay = (taskManager.microsToNextTask() / 1000UL);
    if (milli_delay > 500) {
        // STMSerial.print("milli_delay: ");
        // STMSerial.println(milli_delay);

        // enable low power mode
        // LowPower.idle(milli_delay);
    }
    taskManager.runLoop();
}