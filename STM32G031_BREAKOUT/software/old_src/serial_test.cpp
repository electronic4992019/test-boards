/**
 * @file serial_test.cpp
 * @brief Contains functions for serial communication testing
 *
 * This file contains functions for testing serial communication using the hardware serial port.
 */
#include <Arduino.h>
#include <STM32LowPower.h>

HardwareSerial STMSerial(PB7, PB6);

void setup() {
    STMSerial.begin(115200);
}

void loop() {
    STMSerial.println("Hello World!");
    delay(1000);
}