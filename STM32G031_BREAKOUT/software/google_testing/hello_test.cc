#include <gtest/gtest.h>
#include <cstdint>
#include "ws2812bled.h"
#include <iostream>
#include <bitset>

using namespace std;

// Demonstrate some basic assertions.
TEST(ws2812b_test, basic_assertions) {
  // Expect two strings not to be equal.
  EXPECT_STRNE("hello", "world");
  // Expect equality.
  EXPECT_EQ(7 * 6, 42);
}

TEST(ws2812b_test, convert_to_binary) {
  uint32_t out = convert(128);
  std::cout << "out (binary) = " << std::bitset<24>(out)  << std::endl;
  cout << "out: " << out << endl;
  EXPECT_EQ(out, 0b1010);
}

TEST(ws2812b_test, test_send_data){
    EXPECT_EQ(WS2812_send_data(0, 0, 0), 0);
    EXPECT_EQ(WS2812_send_data(255, 255, 255), 1);
}