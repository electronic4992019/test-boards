/* Testing PWM signal for 12 V computer fan with 4 pins */

/*
  Note: Please verify that 'pin' used for PWM has HardwareTimer capability for your board
  This is specially true for F1 serie (BluePill, ...)
*/

#include <Arduino.h>
#include <TaskManagerIO.h>

#define PIN_FAN PB0
#define PIN_LED PC6
#define PIN_BUTTON PA0

HardwareTimer * LedTimer = nullptr;
HardwareTimer * FanTimer = nullptr;
uint32_t LedChanel = 0;
uint32_t FanChanel = 0;

uint32_t dutycycle = 10;


/**
 * @brief Class to handle events triggered by a button
 */
class EventButton : public BaseEvent {
  public:
    /**
     * @brief Method to execute the button event
     */
    void exec() override {
        dutycycle += 10;
        if (dutycycle > 100) {
            dutycycle = 10;
        }
        LedTimer->setCaptureCompare(LedChanel, dutycycle, PERCENT_COMPARE_FORMAT);
        FanTimer->setCaptureCompare(FanChanel, dutycycle, PERCENT_COMPARE_FORMAT);
    }

    /**
     * @brief Method to return the time of the next check
     * @return Time of the next check
     */
    uint32_t timeOfNextCheck() override {
        return 100000000L;
    }
};

EventButton btn_event;

void onInteruptButton() {
    btn_event.markTriggeredAndNotify();
}



void setup() {
    // no need to configure pin, it will be done by HardwareTimer configuration
    // Automatically retrieve TIM instance and channel associated to pin
    // This is used to be compatible with all STM32 series automatically.
    TIM_TypeDef *Instance = (TIM_TypeDef *)pinmap_peripheral(digitalPinToPinName(PIN_LED), PinMap_PWM);
    LedChanel = STM_PIN_CHANNEL(pinmap_function(digitalPinToPinName(PIN_LED), PinMap_PWM));

    // Instantiate HardwareTimer object. Thanks to 'new' instantiation, HardwareTimer is not destructed when setup() function is finished.
    LedTimer = new HardwareTimer(Instance);

    // Configure and start PWM
    LedTimer->setPWM(LedChanel, PIN_LED, 5, dutycycle); // 5 Hertz, 10% dutycycle

    TIM_TypeDef *Instance2 = (TIM_TypeDef *)pinmap_peripheral(digitalPinToPinName(PIN_FAN), PinMap_PWM);
    FanChanel = STM_PIN_CHANNEL(pinmap_function(digitalPinToPinName(PIN_FAN), PinMap_PWM));

    // Instantiate HardwareTimer object. Thanks to 'new' instantiation, HardwareTimer is not destructed when setup() function is finished.
    FanTimer= new HardwareTimer(Instance2);

    // Configure and start PWM
    FanTimer->setPWM(FanChanel, PIN_FAN, 25000, dutycycle); // 25 KHZ 10% dutycycle

    pinMode(PIN_BUTTON, INPUT_PULLUP);
    attachInterrupt(digitalPinToInterrupt(PIN_BUTTON), onInteruptButton, FALLING);
    taskManager.registerEvent(&btn_event);
}

void loop() {
    taskManager.runLoop();
}





