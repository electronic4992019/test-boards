
#include "ws2812bled.h"



uint32_t convert(uint8_t data)
{
    uint32_t out=0;
    for(uint8_t mask = 0x80; mask; mask >>= 1)
    {
        out=out<<3;
        if (data & mask)
        {
            out = out | 0B110;//Bit high
        }
        else
        {
            out = out | 0B100;// bit low
        }
    }
    return out;
}

unsigned char WS2812_send_data(unsigned char r, unsigned char g, unsigned char b) {
    unsigned char s = 24;

    unsigned long value = 0x00000000;

    value = (((unsigned long) g << 16) | ((unsigned long) r << 8) | ((unsigned long) b));

    while (s > 0) {
        if ((value & 0x800000) != false) {
            return 1;
        } else {
            return 0;
        }
        value <<= 1;
        s--;
    }
}
