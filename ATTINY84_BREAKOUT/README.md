# ATTINY84 Breakout board

A small board for breaking out ATTINY 84

All pins are exposed, except PA3 used for a LED

![pcb-design](images/pcb-design.png)

<!-- ![pcb-design](images/pcb_photo.jpg) -->

![pcb-design](images/pcb_microscope.jpg)
