#ifndef SOFTWARE_TASK_BINK_H
#define SOFTWARE_TASK_BINK_H

#include "Arduino.h"
#include <TaskScheduler.h>

// Task callback
void TaskCallbackBlinkForever();

Task TaskBlinkForever(1000, TASK_FOREVER, &TaskCallbackBlinkForever);
Scheduler runner;

void setup();
void loop();

#endif //SOFTWARE_TASK_BINK_H
