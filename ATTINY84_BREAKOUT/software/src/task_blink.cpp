
#include "task_bink.h"

void setup(){
    pinMode(PA3, OUTPUT);

    runner.init();
    runner.addTask(TaskBlinkForever);
    TaskBlinkForever.enable();
}

void loop(){
    runner.execute();
}

bool b_led_status = LOW;
void TaskCallbackBlinkForever(){
    b_led_status = b_led_status == LOW ? HIGH : LOW;
    digitalWrite(PA3, b_led_status);
}