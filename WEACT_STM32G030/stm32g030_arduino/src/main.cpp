#include <Arduino.h>

HardwareSerial Serial1(PB7, PB6);

void setup() {
    Serial1.begin(115200);
    Serial1.println("Hello World!");
    pinMode(PA_4, OUTPUT);
}

void loop() {
    delay(1000);
    Serial1.println("Hello You");
    digitalWrite(PA_4, HIGH);
    delay(1000);
    digitalWrite(PA_4, LOW);
}