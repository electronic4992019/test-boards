#include <Arduino.h>
#include "TaskManagerIO.h"
//#include "STM32LowPower.h"

#define PIN_LED PIN_A5

uint32_t time_to_next_task = 0;

void doBlink() {
    digitalWrite(PIN_LED, !digitalRead(PIN_LED));
}

void setup() {
    pinMode(PIN_LED, OUTPUT);
    digitalWrite(PIN_LED, LOW);

    //LowPower.begin();
    taskManager.schedule(repeatMillis(1000), doBlink);
}

void loop() {
    taskManager.runLoop();
//    time_to_next_task = taskManager.microsToNextTask();
//    if (time_to_next_task > 100) {
//        LowPower.deepSleep(time_to_next_task);
//    }
}